var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var browsersync = require('browser-sync');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var cssnano = require('gulp-cssnano');
var minify = require('gulp-minify');
var runSequence = require('run-sequence');

// Show notify error
var onError = function(err) {
    notify.onError({
      title:    "Gulp",
      subtitle: "Failure!",
      message:  "Error: <%= error.message %>",
      sound:    "Basso"
    })(err);
    this.emit('end');
};

var sassOptions = {
    outputStyle: 'expanded'
  };
  
var prefixerOptions = {
    browsers: ['last 2 versions']
};

var browsersyncOptions = {
    "server" : "./public"    
}

// Compile SCSS 
gulp.task('scss', () => {
    return gulp.src( 'src/scss/main.scss' )
    .pipe(plumber({errorHandler: onError}))
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions))
    .pipe(autoprefixer(prefixerOptions))
    .pipe(rename('main.css'))
    .pipe(gulp.dest('public/assets/css'))
    .pipe(cssnano( { autoprefixer : { remove: false } , zindex: false } ))
    .pipe(rename({ suffix : '.min' }))
    .pipe(gulp.dest('public/assets/css'))
    .pipe(browsersync.stream());
});

// Compile JS
gulp.task('js',() => {
    return gulp.src('src/js/main.js')
    .pipe(plumber({errorHandler: onError}))
    .pipe(minify({
        ext:{            
            min : '.min.js'
        }        
    }))    
    .pipe(gulp.dest('public/assets/js/'))
    .pipe(browsersync.stream());
});
// Serve and watch 
gulp.task('serve', () => {
    browsersync.init(browsersyncOptions);
    gulp.watch('src/js/*.js', ['js']);
    gulp.watch('src/scss/*.scss', ['scss']);
    gulp.watch('public/*.html').on('change',browsersync.reload);
})



gulp.task('default', function(done) {
    runSequence('scss', 'js','serve' , done);
});